<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('memberid')->nullable();
            $table->string('nick_name')->nullable();
            $table->string('photo')->nullable();
            $table->text('address')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('hp')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('birth_place')->nullable();
            $table->enum('gender',['Pria','Wanita'])->nullable();
            $table->string('education')->nullable();
            $table->string('status')->nullable();
            $table->string('job')->nullable();
            $table->string('skill')->nullable();
            $table->string('fb')->nullable();
            $table->enum('position',[
                'Master',
                'Admin',
                'Member',
            ])->nullable();
            $table->timestamps();

            $table->foreign('user_id')
           ->references('id')
           ->on('users')
           ->onUpdate('cascade')
           ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
