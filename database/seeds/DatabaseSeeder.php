<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ProvincesTableSeeder::class,
            CitiesTableSeeder::class,
            UsersTableSeeder::class,
            RolesTableSeeder::class,
            RoleUsersTableSeeder::class,
            MembersTableSeeder::class,
        ]);
    }
}
