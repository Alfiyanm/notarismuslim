<?php

Route::get('/', function () {
    return view('dashboard/allusers');
});

Route::get('/user-profile', function () {
    return view('dashboard/yourprofile');
});

Route::get('/all-users', function () {
    return view('dashboard/allusers');
});

Route::get('/login', function () {
    return view('dashboard/login');
});

Route::get('/register', function () {
    return view('dashboard/register');
});

// Route::resource('reg', 'Auth\MyRegisterController');
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
