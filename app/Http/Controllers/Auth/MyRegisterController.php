<?php

namespace App\Http\Controllers\Auth;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\RoleUser;
use App\Models\Member;
use App\Models\City;
use Illuminate\Support\Facades\Auth;

class MyRegisterController extends Controller
{
    protected $redirectTo = '/home';
    
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function create()
    {
        $city = City::get();

        return view('auth.register',compact('city'));
    }
    
    public function store(Request $request)
    {
        $user = new User;

        $request->validate([
            // 'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
            'hp' => 'required|min:10|max:15',
            // 'city' => 'required',
            // 'ktp' => 'required'
        ]);
        
        $user->uuid = Uuid::uuid4();
        $user->name = 'belum isi nama!';
        $user->email = $request->email;
        $user->email_verified_at = now();
        $user->password = bcrypt($request->password);
        $user->save();
        

        Member::create(
            [
                'user_id'=>$user->id,
                // 'city' => $request->city,
                // 'ktp' => $request->ktp,
                'hp' => $request->hp,
                'position' => 'Member',
            ]
        );
        RoleUser::create(['user_id' => $user->id,'role_id' => 3]);
        Auth::loginUsingId($user->id);
        
        return redirect()->route('home'); 
    }
}
