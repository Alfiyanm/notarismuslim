<?php

namespace App\Http\Controllers\Dashboard\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Member;

class ProfileController extends Controller
{
    public function show($id)
    {
        $user = User::where('uuid',$id)->first();
        $member = Member::where('user_id',$user->id)->first();

        // dd($user);

        return view('dashboard.member.user.profile.detail_profile',compact('user','member'));
    }
    
    public function edit($id)
    {
        $user = User::where('uuid',$id)->first();
        $member = $user->member()->first();

        return view('dashboard.member.user.profile.edit_profile',compact('user','member'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required'
        ]);
        
        $user = User::where('uuid',$id)->first();
        $member = $user->member()->first();

        //============ User =========
        $user->name = $request->name;
        $user->email = $request->email;
        
        $user->update();

        //=========== member =============
        $member->province = $request->province;
        $member->address = $request->address;
        $member->hp = $request->hp;
        $member->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $member->birth_place = $request->birth_place;
        $member->gender = $request->gender;

        if ($request->hasFile('photo')) 
        {
            $photo = $request->file('photo');
            $photoName = $user->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/member',$photoName);
            $member->photo = $photoName;
        }
        $member->update();

        return redirect()->back()->with('success','Edit Success');
    }

    public function passwordEdit($uuid)
    {
        $user = User::where('uuid',$uuid)->first();

        return view('dashboard.member.user.profile.edit_password',compact('user'));
    }

    public function passwordUpdate(Request $request, $id)
    {
        $user = User::where('uuid',$id)->first();
        
        if ($request->password == $request->password2 && strlen($request->password) >= 8) {
            $user->password = bcrypt($request->password);
            $user->update();

            return redirect()->back()->with('success','Berhasil Merubah Password');
        }else{
            return redirect()->back()->with('info','Password tidak sama atau kurang dari 8 karakter');
        }
        
    }
}
