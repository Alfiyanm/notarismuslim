<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = ['id'];
    protected $table = 'cities';

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }
}
